# slackbot_template

Contains boilerplate for slack website/URL monitoring service.
Supports:
1. Multiple endpoints + http(s), and specific ports [using python requests]
2. Queryable interface / presence on slack channel
3. Cron-able to check endpoints every N min.



# etl-bots

Variety of bots monitoring Website and Services

## REQUIREMENTS
	pip install slackbot slackclient


ServerBot:
	- Runs automated HTTP accessibility test
	cronbot.py -- Added as a cron-able task on interval
	to run --> python mybot.py
		--Inherits from mybot/plugins



Cron:
	If a bot is to be run on an interval:
	crontab -e
	*/5 * * * * /usr/bin/python </path/to/git/>/serverbot/cronbot.py



Watchdog:
 -- Watchdogs can be copied from the Template directory --> Udpate to include relevant details

Then:
	chmod +x watchdog.sh
	crontab -e
	*/5 * * * * </path/to/git/>serverbot/watchdog.sh