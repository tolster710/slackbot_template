from slackbot.bot import respond_to
from slackbot.bot import listen_to
import re 
import requests
from time import sleep
from random import randint

TIMEOUT1=5
TIMEOUT2=13


urls = {'hhh-dev':"http://hhh-dev.getdrinkmate.com:8000",
       'fz-dev':'http://sysdev.fretzealot.com',
       'fz-prod':'http://elb.fretzealot.com',
       'fz-site':'http://www.fretzealot.com'}

def check_status(_url):
    try:
        r=requests.get(_url, timeout=TIMEOUT1)
        if r.status_code == 200 or r.status_code == 401:
            return 1
        print("2nd Attempt at: " + _url)
        r=requests.get(_url, timeout=TIMEOUT2)
        if r.status_code == 200 or r.status_code == 401:
            return 1
        return 0
    except:
        return 0
def check_all_urls(udict):
    o_dict={}
    for u in udict.keys():
        _u = udict[u]
        _res=check_status(_u)
        o_dict.update({u:_res})
    return o_dict
def format_response(o_dict):
	_success=[]
	_fail=[]
	msg=''
	for k in o_dict:
		_d = o_dict.get(k)
		if _d:
			_success.append(k)
		else:
			_fail.append(k)
	if len(_fail) == 0:
		msg = "All Systems operational"
		asg=True
	else:
		msg = "Failed resources:  "
		asg=False
		for k in _fail:
			msg+=" -"+str(k)+"-  "
	return msg, asg
def get_ip():
	r=requests.get("http://ifconfig.me")
	ip = r.text
	return ip

def run_tests(print_response=False):
	o_dict = check_all_urls(urls)
	resp, asg = format_response(o_dict)
	if print_response:
		return resp
	elif asg == False:
		bot._client.rtm_send_message('#server-status', msg)

def get_salutation():
	_list = ["sir", "boss", "chap"]
	return _list[randint(0,len(_list)-1)]


@respond_to('damage report', re.IGNORECASE)
def respond_with_server_statuss(message):
	print("RECEIVED SOMETHING-")
	salutation = 'Of course, '+ get_salutation() + '. Running tests now, give me a minute...'
	message.reply(salutation)
	resp= run_tests(True)
	message.reply(resp)

@respond_to('status report', re.IGNORECASE)
def respond_with_server_statuss(message):
	print("RECEIVED SOMETHING-")
	salutation = 'Of course, '+ get_salutation() + '. Running tests now, give me a minute...'
	message.reply(salutation)
	resp= run_tests(True)
	message.reply(resp)


@respond_to('IP', re.IGNORECASE)
def respond_with_server_ip(message):
	print("RECEIVED IP Request-")
	#message.reply('Of course my dude, running tests now, give me a minute...')
	ip = get_ip()
	msg = "my IP is: "+ str(ip)
	message.reply(msg)
