#!/bin/sh
if ps -ef | grep -v grep | grep "serverbot.py" ; then
        exit 0
else
	cd /path/to/git/serverbot/
        /usr/bin/python /path/to/git/serverbot/serverbot.py >> /home/pi/logs/serverbot.log 2>&1
        exit 0
fi
