from slackclient import SlackClient
import requests
from time import sleep
from time import time

TIMEOUT1=25
TIMEOUT2=40
# The 2 lines below work to open the json file and read the json file
# This is where the bot is very bare, as you need to download the list of users on your team

#STAT_FILE=
#TODO - Add stats if need be

#the next few lines are for Slack communication
token = "xoxb-YOURTOKENHERE" # found at https://api.slack.com/#auth)
sc = SlackClient(token)
chan = "#website"  #Update with desired channel

urls = {'dm-map':'http://map.getdrinkmate.com',
       'fz-dev':'http://sysdev.fretzealot.com',
       'fz-prod':'http://elb.fretzealot.com',
       'fz-site':'http://www.fretzealot.com/faq'}

def check_status(_url):
    try:
        r=requests.get(_url, timeout=TIMEOUT1)
        if r.status_code == 200 or r.status_code == 401:
            return 1
        r=requests.get(_url, timeout=TIMEOUT2)
        if r.status_code == 200 or r.status_code == 401:
            return 1
        return 0
    except:
        return 0
def check_all_urls(udict):
    failed=[]
    o_dict={}
    for u in udict.keys():
        _u = udict[u]
        _res=check_status(_u)
        if _res ==0:
            failed.append(u)
        o_dict.update({u:_res})
    if len(failed)>0:
        sleep(45)
        for f in failed:
            _u = udict[u]
            _res = check_status(_u)
            o_dict.update({u:_res})
    return o_dict
def format_response(o_dict):
	_success=[]
	_fail=[]
	msg=''
	for k in o_dict:
		_d = o_dict.get(k)
		if _d:
			_success.append(k)
		else:
			_fail.append(k)
	if len(_fail) == 0:
		msg = "All Systems operational"
		asg=True
	else:
		msg = "Failed resources: "
		asg=False
		for k in _fail:
			msg+=" -"+str(k)+"- "
	return msg, asg

def run_tests(print_response=False):
	o_dict = check_all_urls(urls)
	resp, asg = format_response(o_dict)
	if print_response:
		return resp
	elif asg == False:
		connect_send_msg(resp)

def connect_send_msg(msg):
	if sc.rtm_connect():
		message = "Hey - Something isn't right "+ str(msg) 
		# The actual command to post the message, which is also printed out to the console
		# The print may be a good way to keep logs :)
		print sc.api_call("chat.postMessage", as_user="true:", channel=chan, text=message)
	
	else:
		print("Connection Failed, invalid token?")

if __name__ == "__main__":
    run_tests()
